#Mobx as a Redux alternative

##Short story of state management.
A while back all of the heavy lifting in a web application was done on a server using
Backend language like Python or PHP. Since server computational resources like CPU or RAM
have to be share between sometime thousand of requests we as developers moved some of
that work to the client browser which gave Frontend Developers more power and as we
know "With great power comes great responsibility".

Now with new generation of JavaScript frameworks like **React**, or **Angular 2** we split
our application into components. It greatly improves separation of conceirns in the application
by isolation of component-specific state ( like autocomplete sugestions for search bar )
in that component. But it does not solves all the problems.

Although in small applications **storing state on component-level** might be enought it is not always the case with growing
amount of sources on the data and task we want we use it for. State can be derived from
server respose, from LocalStorage or it can be a result of some user interaction like
changing a route, or filling out the form. **That state might be a conceirn of more then
one hardly related component.**

This caused state containers to be born. First there was **Flux architecture** which
was more of a proof of concept then a real solution since it was complicated and
had a steep learning curve. Now we have to competing solutions **Redux** and **MobX**, and
although the foremost had a little quick start, MobX is gaining on popularity and dedicated contributors.

##What Redux stand about.
Redux is known from being a small and intuitive implementation of Flux architecture.
**It is based on hot concepts in developers' world like Functional Programming and Immutability.**
There are **three priciples** to Redux:

- Store as a single source of truth, which means all of are application data is stored
inside of a state tree which allows for easier debugging since we know that all information
comes from there.
- Store is **read-only**, which means it cannot be changed directly but only through dispatching
actions that represent a change in your application ecosystem.
- Changes are made using pure functions. Every dispatch actions flows through the set of reducers, **pure functions**
which take the previous part of state and an action with optional payload and create new state from that by returing it.
From the definition of pure function emerges a rule that reducers **cannot have side effects** like asychronous request or writing to LocalStorage.
They also **cannot mutate** element of the state, they have to create new element that contains the data of the previous one.

Those principles combined create a pipeline for indroducing a change in application state that is ease to reason about, debug and extend.

##So what MobX has to offer?
MobX represents a way that is as different from Redux as it's possible. It draws from **Object Oriented Programming**
and it is an implementation of the **Observer Pattern**. It's also all about mutating state, tracking those mutations and
automatically updating all component interested in those changes. Oppose to Redux whay there is more then one store in MobX.
In fact you have as many stores as you find necessary starting with at least two. One store for you domain data (i.e. products sold in case of online shop),
and the other for the UI state (i.e. is modal open, is drop down menu extended).

Stores are just simple object in which we mark some properties as observable so MobX which start tracking the references to them.
**This results in lack of need for explicit declaration on what properties we observe**. MobX will figure that one out by first running
a fragment of code an registering which properties where used inside of it. Next time those properties are change it will rerun the code
fragment. **That prevents unecessary updates** when there is change in property that was not used by anyone.

Although it allows changes to store form anywhere in your application **it encourages use on mechanism especially for introducing those changes**.
Stores can have methods delared as actions. There are not pure functions however there is an analogy to reducers in Redux. An action have
a store in context and takes optional parameter and introduce changes in this store. This way a **side effects in action are limited to changes
in the store**.

##Why you should try MobX in you next project instead of Redux.
Setting up Redux in your project although intutive can take some time as **there is a lot of boilerplate code**. You have to create reducers, actions creators,
wire store with some third-party middlewares (i.e. redux-thunk, redux-sage) for your state container to actually be able to do anything for you.
With MobX the **setup couldn't be easier**. It is just writing a simple class with observable properties you need and pass it down to your components tree.
With Redux there in **no out-of-the-box mechanism for asychronous action**. It requires you to use middlewares with **thunks or sagas**.
MobX comes **with batteries included** and asychronous actions done simple and intutive just as they should be.
With side effects in actions one might be worried about the testibility but there is **no problem with testing** MobX stores.
You initialize store, perform an action and make assersions about store properties since there are the only things to change.
