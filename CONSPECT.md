#Mobx Presentation


##Introduction
I think it's not wrong to say that Mobx takes opposite approach to state management then Redux.
Just as in Redux is all about non-mutability as in Mobx object properties should be change
not the whole object.

The store declares which properties of his are observable so other parts of application
are notified when those properties change. It takes more Object-Oriented approach as
oppose to Redux's Fuctional approach. To do this we can use `@observable` decorator.

```
 class TodoStore {
   @observable todos = [];
   @action createTodo(todo) {
    this.todos.push(todo);
   }
 }
```

In the this example we could of course modify Todos' list by directly pushing new todo
```
  todoStoreInstance.todos.push(todo);
```
but to explicly highlight code fragments in which some observable property of an object
is change we use another decorator named `@action`. Action can also take form of
higher-order function:
```
  fetchTodos() {
    fetch('someurl').then(action((todos) => {
      this.todos = todos;
    }));
  }
```
Because in that a callback is a place where we modify observable so we pass to Promise
a function wrapped in `action` and not decorating fetchTodos method because it does not
modify observable directly.

To see how in works in pure JS before mixing it with React here goes example:
```
class Store {
  @observable counter = 0;
  @action increment() {
    this.counter += 1;
  }
}

const store = new Store();
autorun(() => {
  console.log(store.counter);
});

setInterval(() => {
  store.increment();
}, 1000);

```
We execute increment action every second. Autorun function will re run each time
any observable it is subsribe too is change. What i mean by subscribe is that
Mobx first runs function and tracks to which properties there were references during
execution later if any of those properties is changed function will be re run so there
is now need for explicit declaration of on what observables we depend on.

There is one more thing to now about before we'll discuss Mobx with React and it is
an computed value. Lets say our stores contains an list of todos and and allows setting a filter
like completed or importand. Our Store'll look something like this

```
const FILTERS = {
  NONE: 0,
  COMPLETED: 1,
  NOTCOMPLETED: 2,
};

class TodoStore {
  @observable todos = [ /* some todos here */ ];
  @observable filter = FILTERS.NONE;

  @action setFilter(filter) {
    this.filter = filter;
  }
}
```

we could of course in our autorun function filter on but it would be suboptimal
if autorun would observe some property different then todos and filter i would
cause filtering list even if nothing in it has changed. The way to do it correctly
is to use computed values. In our store we add:

```
class TodoStore {
  /* code from before */

  @computed get filteredTodos() {
    switch(this.filter) {
      case FILTERS.COMPLETED:
        return this.todos.filter(todo => todo.isCompleted);
      case FILTERS.NOTCOMPLETED:
        return this.todos.filter(todo => !todo.isCompleted);
      default:
        return this.todos.peek();
    }
  }
}
```

The computed value declared here will only rerun if some property used in the previous
execution has change. It's worth pointing that when u push to an observable list a simple
object Mobx automatically makes all of its properties observable recursively.
So back to the the fuction will no re run for example when one of the todoes changes it's
state to `isCompleted: true` if there is no current filter because this property was not used
in previous execution. We also should be aware that this.todos is an instanceOf `ObservableArray`
but the product of filter function is not so for the sake of uniformity of our `filteredTodos`
property we use `peek` function which changes `ObservableArray` to regular array.
However it's elements are still observables.

##React way
So here it is. A way to connect Mobx with React. But first some theory. In Redux we have got one big store
which is not in case in Mobx. In the simplest case we need 2 stores. One for our domain data like
previously shown TodoStore, and one for UI data. Things like isThisModal open isThisMenu extended etc.

The same as with Redux we need another library it this case called `mobx-react`.
First of all we need `<Provider />` component to pass down our stores inside of the React context.

```
<Provider UIStore={UIStore} TodoStore={TodoStore}>
  <App />
</Provider>
```

There are 2 Higher-Order components in that library that we will use.

###Observer
First of them is `observer`. When our component is class-based we can use it as decorator, otherwise as a function.

```
@observer class App {
  /* content */
}

/*** OR ***/

const App = () => (
 /* content */
);

observer(App);
```

It goes throught all props and state watching every observable in there. On mount in
renders component and tracks which props or states where used and if any of them changes
it will rerender component. Without it all `Observable` pass to the component will be treated
as regular paramater.

###Inject
The other HOC is `inject` which is a way to access stores in context placed by `<Provider />` it
is a little similary to `connect`. It is always used in combination with `observer`.

```
@inject('UIStore', 'TodoStore') @observer class App {
  /* content */
}

/*** OR ***/

const App = () => (
 /* content */
);

inject('UIStore', 'TodoStore')(observer(App));
```


##Examples?


##Best Practices

One of the important things is to make sure that in your main file execute `useStrict` MobX function.
It assures for your sanity that the observable properties are not change outsize `action` decorated functions.

MobX obviously cannot observe primitive values like Number you should way with dereferancing value as long as possible
to be sure you to not cause unecessary rerender.

Although you can do a lot of things in your actions you should limit side-effects to changing the store to which
action relates to. If you want to introduce changes in different store you should invoke actions of that store.
All other side-effects like writing to localstorage should be done in reactions like autorun.

###Domain Objects

You might want only some of your properties to be observable. If your store is made of list (stick with the example) Todos,
single Todo does not need to be simple JS Object. We can create something to is refered to as Domain Object.

```
class Todo {
  title = '';
  priority = 0;
  @observable isCompleted = false;
}
```

First of all this way we make only some of our properties observable, because i.e. title does not
change so MobX does not have to care about it. Additionally Todo can have it's own actions now.
It is common practice for a Domain Object to have a reference to the store.

```
class Todo {
  /* content */
  constructor(store, options) {
    this.store = store;
  }
}
```

This way single Todo can delegate part of the functionality to the store. For example logic of making
Asynchronous Requests.


###Communication between stores
Sometime you might find yourself in situation where u need to access one store property from the other or call it's action.
One way to achive that is to export from each Store file a singleton and import it directly to the other store.

```
/* stores/TodoStore.js */
import UserStore from './UserStore';
class TodoStore {

}

export default new TodoStore();

/* stores/UserStore.js */
import TodoStore from './TodoStore';

class UserStore {

}
export default new UserStore();

```

The other is using some dependency injection library. Which is out of the scope of this presentation.

The last and the one promoted is to use a Root Store,
a store that have reference to all other stores and those store have reference to him.
Accessing a userStore from todoStore could look something like this:

```
/* stores/TodoStore.js */
class TodoStore {
    constructor(root) {
      this.root = root;
    }
    @computed get todosForCurrentUser() {
      return this.todos.filter(todo => todo.owner === this.root.userStore.activeUser);
    }
}

/* index.js */
const rootStore = new RootStore();
const userStore = new UserStore(rootStore);
const todoStore = new TodoStore(rootStore);

rootStore.userStore = userStore;
rootStore.todoStore = todoStore;
```

The advantages of last approach over the first one is that in this case during
testing we have a way of mocking userStore without messing with module loading.


##Conclusion
Writing things with complicated state with Mobx is super fast and easy
if we want to add new property to `Todo` (i.e. isInProgress) all we have to do is
to add it to Domain Object and decorate it with `observable` and we can use it in
our component.

It also is very good for items with complicated relationship because it does not
impose normalizing your data unlike Redux.
