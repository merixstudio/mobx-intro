import { observable, action } from 'mobx';

export default class UIStore {
  @observable hoveredList;
  @observable draggedTask;
  @observable isDragging;

  constructor(rootStore) {
    this.root = rootStore;
  }
  @action listDragOver(list) {
    this.hoveredList = list;
  }

  @action taskDrag(task) {
    this.isDragging = true;
    this.draggedTask = task;
  }

  @action taskDrop() {
    this.draggedTask.changeStage(this.hoveredList);
    this.isDragging = false;
    this.draggedTask = null;
    this.hoveredList = null;
  }
}
