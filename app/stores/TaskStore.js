import { observable, computed, action, autorun } from 'mobx';

import Task, { STAGE, PRIORITY } from 'objects/Task';

const byChangeDate = (a, b) => b.changeDate.getTime() - a.changeDate.getTime();
const byPriority = (a, b) => b.priority - a.priority;

export default class Store {
  @observable tasks = [];

  constructor() {
    const stored = JSON.parse(localStorage.getItem('tasks'));
    this.tasks = stored ? stored.map(taskString => new Task(this, taskString)) : [];
    autorun(() => {
      localStorage.setItem('tasks', JSON.stringify(this.tasks));
    });
  }

  createNewTask(author, title) {
    this.addTask(new Task(this, {
      id: this.tasks.length ? this.tasks[this.tasks.length - 1].id + 1 : 0,
      author,
      title,
      stage: STAGE.TODO,
      priority: PRIORITY.LOW,
      changeDate: new Date(),
    }));
  }

  @action addTask(task) {
    this.tasks.push(task);
  }
  @action removeTask(task) {
    this.tasks = this.tasks.filter(t => t !== task);
  }

  @computed get todoTasks() {
    return this.tasks.filter(task => task.stage === STAGE.TODO).sort(byPriority);
  }
  @computed get inProgressTasks() {
    return this.tasks.filter(task => task.stage === STAGE.INPROGRESS).sort(byPriority);
  }
  @computed get qaTasks() {
    return this.tasks.filter(task => task.stage === STAGE.QA).sort(byPriority);
  }
  @computed get doneTasks() {
    return this.tasks.filter(task => task.stage === STAGE.DONE).sort(byChangeDate);
  }
  @computed get criticalTasks() {
    return this.tasks.filter(task => task.priority === PRIORITY.CRITICAL);
  }
  @computed get highTasks() {
    return this.tasks.filter(task => task.priority === PRIORITY.HIGH);
  }
  @computed get mediumTasks() {
    return this.tasks.filter(task => task.priority === PRIORITY.MEDIUM);
  }
  @computed get lowTasks() {
    return this.tasks.filter(task => task.priority === PRIORITY.LOW);
  }
}
