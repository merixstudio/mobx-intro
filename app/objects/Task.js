import { observable, action } from 'mobx';

export const STAGE = {
  TODO: 'TODO',
  INPROGRESS: 'INPROGRESS',
  QA: 'QA',
  DONE: 'DONE',
};
export const PRIORITY = {
  CRITICAL: 4,
  HIGH: 3,
  MEDIUM: 2,
  LOW: 1,
};

export function priorityToString(priority) {
  switch (priority) {
    case PRIORITY.CRITICAL: return 'critical';
    case PRIORITY.HIGH: return 'high';
    case PRIORITY.MEDIUM: return 'medium';
    case PRIORITY.LOW: return 'low';
    default: return '';
  }
}

export default class Task {
  title = '';
  author = '';
  @observable priority;
  @observable stage;
  @observable changeDate;

  constructor(store, params) {
    this.store = store;
    Object.keys(params).forEach((key) => {
      this[key] = params[key];
    });
    this.changeDate = new Date(params.changeDate);
  }

  @action changePriority(priority) {
    if (this.priority === priority) return;
    this.priority = priority;
    this.changeDate = new Date();
  }
  @action changeStage(stage) {
    if (this.stage === stage) return;
    this.stage = stage;
    this.changeDate = new Date();
  }

  removeTask() {
    this.store.removeTask(this);
  }

  toJSON() {
    const that = { ...this };
    delete that.store;
    return that;
  }
}
