import React from 'react';
import { useStrict } from 'mobx';
import { Provider } from 'mobx-react';
import ReactDOM from 'react-dom';

import App from 'components/App';
import TaskStore from 'stores/TaskStore';
import UIStore from 'stores/UIStore';

import './styles/main.scss';

const taskStore = new TaskStore();
const uiStore = new UIStore();

useStrict();

ReactDOM.render(
  <Provider taskStore={taskStore} uiStore={uiStore} >
    <App />
  </Provider>,
  document.querySelector('#root'),
);
