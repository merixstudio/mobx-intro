import React from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';

import ChangePriority from 'components/ChangePriority';
import TaskObject, { priorityToString, STAGE } from 'objects/Task';
import UIStore from 'stores/UIStore';

function onDrag(uiStore, task) {
  uiStore.taskDrag(task);
}

const Task = ({ uiStore, task, draggable }) => (
  <div
    draggable={draggable}
    onDragStart={event => draggable && onDrag(uiStore, task, event)}
    className={`
      task
      task--${task.stage.toLowerCase()}
      task--${priorityToString(task.priority)}
      ${draggable ? 'task--draggable' : ''}
    `}
  >
    <h3 className="task__name">{ task.title }</h3>
    <p className="task__stage">Stage: { task.stage }</p>
    { task.stage !== STAGE.DONE ? [
      <ChangePriority key="changePriority" task={task} />,
      <p key="author" className="task__author">Author: { task.author }</p>,
    ] : (
      <p className="task__date">{ task.changeDate.toLocaleString() }</p>
    )}
    <button className="task__delete" onClick={() => task.removeTask()}>X</button>
  </div>
);


Task.propTypes = {
  uiStore: PropTypes.instanceOf(UIStore).isRequired,
  task: PropTypes.instanceOf(TaskObject).isRequired,
  draggable: PropTypes.bool,
};

Task.defaultProps = {
  draggable: false,
};

export default inject('uiStore')(observer(Task));
