import React from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';

import UIStore from 'stores/UIStore';
import Task from 'components/Task';
import TaskObject from 'objects/Task';

function onDrop(uiStore) {
  uiStore.taskDrop();
}
function onDragOver(uiStore, stage, event) {
  event.preventDefault();
  uiStore.listDragOver(stage);
}

const TaskList = ({ tasks, title, uiStore, droppable, stage }) => {
  let additionalClass = '';
  if (droppable && uiStore.isDragging) {
    if (uiStore.hoveredList === stage) {
      additionalClass = 'task-list--highlighted-strong';
    } else {
      additionalClass = 'task-list--highlighted';
    }
  }

  return (
    <div
      className={`task-list ${stage ? `task-list--${stage.toLowerCase()}` : ''} ${additionalClass}`}
      onDrop={() => onDrop(uiStore)}
      onDragOver={event => droppable && onDragOver(uiStore, stage, event)}
    >
      <h2 className="task-list__title">{ title }</h2>
      <ul className="task-list__list" >
        {
          tasks.map(task => (
            <li key={task.id} className="task-list__element">
              <Task task={task} draggable={droppable} />
            </li>
          ))
        }
      </ul>
    </div>
  );
};

TaskList.propTypes = {
  uiStore: PropTypes.instanceOf(UIStore).isRequired,
  title: PropTypes.string.isRequired,
  tasks: PropTypes.arrayOf(PropTypes.instanceOf(TaskObject)).isRequired,
  stage: PropTypes.string,
  droppable: PropTypes.bool,
};

TaskList.defaultProps = {
  droppable: false,
  stage: null,
};

export default inject('uiStore')(observer(TaskList));
