import React from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';

import TaskStore from 'stores/TaskStore';
import { STAGE } from 'objects/Task';
import TaskList from 'components/TaskList';
import TaskForm from 'components/TaskForm';

/* eslint-disable react/prefer-stateless-function */
class App extends React.Component {
  render() {
    const { taskStore } = this.props;
    return (
      <div className="app">
        <div className="app__container">
          <div className="app__row">
            <TaskList title="To do" stage={STAGE.TODO} tasks={taskStore.todoTasks} droppable />
            <TaskList title="In Progress" stage={STAGE.INPROGRESS} tasks={taskStore.inProgressTasks} droppable />
            <TaskList title="QA" stage={STAGE.QA} tasks={taskStore.qaTasks} droppable />
            <TaskList title="Done" stage={STAGE.DONE} tasks={taskStore.doneTasks} droppable />
            <div className="app__divider" />
            <TaskList title="Critical" tasks={taskStore.criticalTasks} />
            <TaskList title="High" tasks={taskStore.highTasks} />
            <TaskList title="Medium" tasks={taskStore.mediumTasks} />
            <TaskList title="Low" tasks={taskStore.lowTasks} />
          </div>
          <TaskForm />
        </div>
      </div>
    );
  }
}
/* eslint-enable react/prefer-stateless-function */

App.propTypes = {
  taskStore: PropTypes.instanceOf(TaskStore).isRequired,
};

export default inject('taskStore')(observer(App));
