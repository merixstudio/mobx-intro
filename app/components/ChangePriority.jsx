import React from 'react';
import { observer } from 'mobx-react';
import PropTypes from 'prop-types';

import Task, { PRIORITY, priorityToString } from 'objects/Task';

function changePriority(task, priority) {
  if (priority !== task.priority) {
    task.changePriority(priority);
  }
}
const prioritiesList = [
  PRIORITY.CRITICAL,
  PRIORITY.HIGH,
  PRIORITY.MEDIUM,
  PRIORITY.LOW,
];
const ChangePriority = ({ task }) => (
  <ul className="change-priority">
    { prioritiesList.map(priority => (
      <li key={priority} className={`change-priority__item change-priority__item--${priorityToString(priority)}`}>
        <button
          className="change-priority__button"
          onClick={() => changePriority(task, priority)}
          disabled={task.priority === priority}
        />
      </li>
    ))}
  </ul>
);

ChangePriority.propTypes = {
  task: PropTypes.instanceOf(Task).isRequired,
};
export default observer(ChangePriority);
