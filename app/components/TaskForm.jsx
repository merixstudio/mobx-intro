import React from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';

import TaskStore from 'stores/TaskStore';

function onSubmit(event, taskStore) {
  event.preventDefault();
  const author = event.target.author.value;
  const title = event.target.title.value;
  if (author && title) {
    taskStore.createNewTask(author, title);
  }
}

const TaskForm = ({ taskStore }) => (
  <form className="task-form" autoComplete="off" onSubmit={event => onSubmit(event, taskStore)}>
    <div className="task-form__input-row">
      <input className="task-form__input" name="author" placeholder="author" />
    </div>
    <div className="task-form__input-row">
      <input className="task-form__input" name="title" placeholder="title" />
    </div>
    <div className="task-form__controls">
      <input className="task-form__submit" value="createTask" type="submit" />
    </div>
  </form>
);

TaskForm.propTypes = {
  taskStore: PropTypes.instanceOf(TaskStore).isRequired,
};


export default inject('taskStore')(observer(TaskForm));
